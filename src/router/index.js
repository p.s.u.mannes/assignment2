//importing router dependencies
import Vue from 'vue'
import VueRouter from 'vue-router'

//telling vue to use the router
Vue.use(VueRouter)

//declaring routes (name = name of page, path = filepath)
const routes = [
    {
        path: '/',
        name: 'Start',
        component: () => import(/* webpackChunkName: "start" */ '../components/Start/Start.vue')
    },

    {
        path: '/question',
        name: 'Question',
        component: () => import(/* webpackChunkName: "question" */ '../components/Question/Question.vue')
    },

    {
        path: '/result',
        name: 'Result',
        component: () => import(/* webpackChunkName: "result" */ '../components/Result/Result.vue')
    }
]

//creating instance of router object using the routes list
const router = new VueRouter ({
    mode: 'history', 
    base: process.env.BASE_URL,
    routes
})

//exporting the router object to be used in app.vue
export default router;