export const TriviaAPI = {
    /*
    For details on the trivia API see https://opentdb.com/

    @param amount = amount of questions
    @param category = category of questions
    @param difficulty = difficulty of questions
    @param token = the API session token, retrieved by fetchSessionKey
    */

    //fetches API trivia data based on user settings
    //user does not need to change settings, the state defaults to 4 questions and any category/difficulty
    fetchQuestions(amount, category, difficulty, token) {
        return fetch(`https://opentdb.com/api.php?amount=${amount}&category=${category}&difficulty=${difficulty}&token=${token}`)
        //return fetch('https://opentdb.com/api.php?amount=10&category=9&difficulty=medium')
            .then(response => response.json())
            .then(data => data.results)
    },
    //fetches session key
    fetchSessionKey() {
        return fetch("https://opentdb.com/api_token.php?command=request")
            .then(response => response.json())
            .then(data => data.token)
    }
}

