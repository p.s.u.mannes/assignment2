// importing dependencies...

//import Vue from 'vue' dependancy
import Vue from 'vue'
//import vuex
import Vuex from 'vuex'
//import API functions
import { TriviaAPI } from "@/api/TriviaAPI"

Vue.use(Vuex)

export default new Vuex.Store({
    //define application state in js object:
    //Setting the DEFAULT STATE:
    state: {
        difficulty: '',
        amount: '4',
        category: '',
        questions: {},
        answers: [],
        correctAnswers: [],
        sessionToken: '',
        score: 0,
        error: ''
    },

    //define mutations (things that change)
    //mutators change the state
    mutations: {
        //function takes state of application
        //payload = desired state, what you want to set state to
        setDifficulty: (state, payload) => {
            state.difficulty = payload
        },
        //updating variable state. state.variable = payload
        setAmount: (state, payload) => {
            state.amount = payload
        },
        //updating variable state. state.variable = payload
        setCategory: (state, payload) => {
            state.category = payload
        },
        //updating variable state. state.variable = payload
        setQuestions: (state, payload) => {
            state.questions = payload
        },
        //updating variable state. state.variable = payload
        setAnswers: (state, payload) => {
            state.answers = payload
        },
        //updating variable state. state.variable = payload
        setError: (state, payload) => {
            state.error = payload
        },
        //updating variable state. state.variable = payload
        setSessionToken: (state, payload) => {
            state.sessionToken = payload
        },
        //adding answers to list array by pushing the payload
        pushAnswer: (state, payload) => {
            state.answers.push(payload)
        },
        //updating variable state. state.variable = payload
        setCorrectAnswer: (state, payload) => {
            state.correctAnswers = payload
        },
        //adding answers to list array by pushing the payload
        pushCorrectAnswer: (state, payload) => {
            state.correctAnswers.push(payload)
        },
        //updating variable state. state.variable = payload
        setScore: (state, payload) => {
            state.score = payload
        }
    },

    //filtering done in getters...g
    //e.g. getting the value of a state after doing operations on it
    getters: {

    },

    // all async methods go here
    actions: {
        /* object destructuring with { } to be able to access commit, state directly

        Fetches the API question data and commits it to the state (see TriviaAPI in @/api/TriviaAPI.js for details)  */
        async fetchQuestions({ commit }) {
            try {
                // state.amount, state.category, state.difficulty
                const questions = await TriviaAPI.fetchQuestions(this.state.amount, this.state.category, this.state.difficulty, this.state.sessionToken)
                commit('setQuestions', questions)
            } catch (e) {
                commit('setError', e.message)
            }
        },

        /* Fetches the session key token, used by the API to generate
        unique questions for up to 6 hours for a user */
        async fetchSessionKey({ commit }) {
            try {
                const sessionToken = await TriviaAPI.fetchSessionKey()
                commit('setSessionToken', sessionToken)
            } catch (e) {
                commit('setError', e.message)
            }
        },

        /* Updates the state amount when the user changes
        the amount */
        async updateAmount({ commit }, payload) {
            try {
                commit('setAmount', payload)
            } catch (e) {
                commit('setError', e.message)
            }
        },

        /* Updates the state category when the user changes
        the category */
        async updateCategory({ commit }, payload) {
            try {
                commit('setCategory', payload)
            } catch (e) {
                commit('setError', e.message)
            }
        },

        /* Updates the state difficulty when the user changes
        the difficulty */
        async updateDifficulty({ commit }, payload) {
            try {
                commit('setDifficulty', payload)
            } catch (e) {
                commit('setError', e.message)
            }
        },

        /* Commits the answer to the answers list */
        async pushAnswer({ commit }, payload) {
            try {
                commit('pushAnswer', payload)
            } catch (e) {
                commit('setError', e.message)
            }
        },

        /* Commits the correct answer to the correctAnswer list */
        async pushCorrectAnswer({ commit }, payload) {
            try {
                commit('pushCorrectAnswer', payload)
            } catch (e) {
                commit('setError', e.message)
            }
        },

        /* Increases the score by 10, used for rewarding the user
        with points for correct trivia answers */
        async increaseScore({ commit }) {
            try {
                commit('setScore', this.state.score + 10)
            } catch (e) {
                commit('setError', e.message)
            }
        },

        /* Resets the score. Not used, but kept regardless for future
        purposes */
        async resetScore({ commit }) {
            try {
                commit('setScore', 0)
            } catch (e) {
                commit('setError', e.message)
            }
        },

        /* Does a soft reset of the state by committing a reset
        to the original state for score, answers, and correct answers.
        Used when the user wants to replay and we do not want to
        remember their previous score and answers. */
        async softResetState({ commit }) {
            try {
                commit('setScore', 0)
                commit('setAnswers', [])
                commit('setCorrectAnswer', [])
            } catch (e) {
                commit('setError', e.message)
            }
        },
    }
})